/**
 * @author Andryushin Dmitriy 16it18k
 */
public class Robot {
    private int x;
    private int y;
    private Direction direction;

    public Robot(int x, int y, Direction direction) {
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    public Robot() {
        this(0, 0, Direction.DOWN);
    }

    public void toMove() {
        switch (direction) {
            case DOWN:
                y--;
                break;

            case LEFT:
                x--;
                break;

            case UP:
                y++;
                break;

            case RIGHT:
                x++;
                break;
        }
    }

    public void toRight() {
        switch (direction) {
            case UP:
                direction = Direction.RIGHT;
                break;

            case RIGHT:
                direction = Direction.DOWN;
                break;

            case DOWN:
                direction = Direction.LEFT;
                break;

            case LEFT:
                direction = Direction.UP;
                break;
        }
    }

    public void toLeft() {
        switch (direction) {
            case UP:
                direction = Direction.LEFT;
                break;

            case LEFT:
                direction = Direction.DOWN;
                break;

            case DOWN:
                direction = Direction.RIGHT;
                break;

            case RIGHT:
                direction = Direction.UP;
                break;
        }
    }

    public void move(int x1, int y1) {
        if (x < x1) {
            while (direction != Direction.RIGHT) {
                toRight();
            }
        } else if (x > x1) {
            while (direction != Direction.LEFT) {
                toLeft();
            }
        }
        while (x != x1) {
            toMove();
        }

        if (y < y1) {
            while (direction != Direction.UP) {
                toRight();
            }
        } else if (y > y1) {
            while (direction != Direction.DOWN) {
                toLeft();
            }
        }
        while (y != y1) {
            toMove();
        }
    }

    public String toString() {
        return "Робот стоит по координатам " + x + " и " + y;
    }
}